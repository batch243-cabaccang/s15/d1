const simpleSample = () => {
  console.log("Hehe");
};

simpleSample();

let productName = "Desktop Compuater";
console.log(productName);
let productPrice = 18999;
console.log(productPrice);
const interest = 3.539;
console.log(interest);

a = 5;
console.log(a);
var a;

{
  const interest = 4.66;
  console.log(interest);
}

let productCode = "DC017",
  productBrand = "Dell";
console.log(productCode, productBrand);

let country = "Philippines";
let province = "Metro Manila";

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early.";
console.log(message);

let count = "26";
let headCount = 26;
console.log(count, headCount);

let grade = 98.7;
console.log(grade);

let planteDistance = 2e10;
console.log(planteDistance);

console.log(count + headCount);

let isMarried = false;
let inGoodConduct = true;

console.log("string" + isMarried);

const grades = [1, 3, 5, 6];
console.log(grade[1]);

const anObject = {
  fullname: "Ako",
  age: 18,
  isMarried: false,
  contact: [123123124123, 12123123],
  address: {
    country: "Philippines",
  },
};
console.log(anObject);

console.log(typeof anObject);

// Constant Objects and Arrays
//     The keyword const is a little misleading.

//     It does not define a constant value. It defines a constant reference to a value.

//     Because of this you can NOT:

//     Reassign a constant value
//     Reassign a constant array
//     Reassign a constant object

//     But you CAN:

//     Change the elements of constant array
//     Change the properties of constant object

const arr1 = ["k", "l", "h"];
console.log(arr1);
arr1[1] = "a";
console.log(arr1);

let spouse = null;
console.log(spouse);

let fullName;
console.log(fullName);
